from main import ConstraintManual, ConstrainedOptimization
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt

# === Setting up a constraint ===
x_arr = np.arange(-2.1, 2.1, 0.001)
arr = [(x, x) for x in x_arr]


def constraint_func(inp):
    x, y = inp
    if y >= x:
        return True
    else:
        return False


req1 = ConstraintManual(array=arr, func=constraint_func)

# === Setting up a second constraint ===
x_arr = np.arange(-2.1, 0, 0.001)
arr = [(x, -x**3-0.5) for x in x_arr]


def constraint_func(inp):
    x, y = inp
    if y >= -x**3-0.5:
        return True
    else:
        return False


req2 = ConstraintManual(array=arr, func=constraint_func)

# === Setting up a third constraint ===
x_arr = np.arange(-2.2, 2.2, 0.001)
arr = [(x, 2*sqrt(x+2.2)) for x in x_arr]


def constraint_func(inp):
    x, y = inp
    if y <= 2*sqrt(x+2.2):
        return True
    else:
        return False


req3 = ConstraintManual(array=arr, func=constraint_func)


# === Updating the domain with constraint ===
co = ConstrainedOptimization(x_min=-2, x_max=2, y_min=0, y_max=10)

plt.title("Before")
co.plot_domain()

co.adapt_domain(req1)
co.adapt_domain(req2)
co.adapt_domain(req3)

plt.title("After")
co.plot_domain()
