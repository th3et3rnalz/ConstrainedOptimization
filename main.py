import numpy as np
from math import sqrt
import matplotlib.pyplot as plt


def plot_list(xy_list, *args, **kwargs):
    x = [point[0] for point in xy_list]
    y = [point[1] for point in xy_list]
    plt.plot(x, y, *args, **kwargs)


class ConstraintManual:
    def __init__(self, array: list, func: callable):
        """
        The is the default class for adding constraints to the ConstrainedOptimization class.

        :param array: list of (x,y) pairs that denote the limit of the constraint. These points should just meet the
           constraint. This list should return true on the constraint function.
        :param func: A function:
           -> Inputs: x and y [list of pairs]
           -> Output: True (when the xy pair satisfies the constraint) and False (when constraint is not satisfied)
        """
        self.array = array
        self.func = func


class Constraint:
    def __init__(self, x: dict = None, y: dict = None):
        """
        The is the default class for adding constraints to the ConstrainedOptimization class.

        :param x: A dict with:
          -> 'expression': 'some_logical_expression_with_x_and_y' This will be translated into the function.
              example: 'x>2*y +2' or 'x>2'
          -> 'x_min': The smallest value of x that should be in the cutting line. No danger exaggerating a bit.
          -> 'x_max': The largest value of x that should be in the cutting line. No danger exaggerating a bit.
          -> 'step': The step size of the line.

        :param y: A dict with:
          -> 'expression': 'some_logical_expression_with_x_and_y' This will be translated into the function.
              example: 'y>2*x +2' or 'y>2'
          -> 'y_min': The smallest value of x that should be in the cutting line. No danger exaggerating a bit.
          -> 'y_max': The largest value of x that should be in the cutting line. No danger exaggerating a bit.
          -> 'step': The step size of the line.
        """
        # ======= Y ========
        if x is None and type(y) == dict:
            def func(inp):
                special_bool = eval(y['expression'], {'x': inp[0], 'y': inp[1]})
                return bool(special_bool)

            self.func = func

            line_str = y['expression']
            for to_remove in ('y', '<=', '>=', '<', '>',):
                line_str = line_str.replace(to_remove, '').strip()

            def line_func(x):
                return eval(line_str, {'x': x})

            x_array = np.arange(y['x_min'], y['x_max'], y['step'])
            xy_arr = [(x, line_func(x)) for x in x_array]
            self.array = xy_arr
        else:
            raise NotImplementedError


class ConstrainedOptimization:
    def __init__(self, x_min, x_max, y_min, y_max, border_size=1000, debug=False):
        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max
        self.debug = debug

        # We must now setup the borders.
        self.domain = []
        for x in np.linspace(x_min, x_max, border_size):
            self.domain.append((x, y_min))

        for y in np.linspace(y_min, y_max, border_size):
            self.domain.append((x_max, y))

        for x in np.linspace(x_max, x_min, border_size):
            self.domain.append((x, y_max))

        for y in np.linspace(y_max, y_min, border_size):
            self.domain.append((x_min, y))

    def plot_domain(self):
        plot_list(self.domain)
        plt.show()

    def adapt_domain(self, cs: Constraint, debug=None):
        if debug is not None:
            self.debug = debug
        # First step is to find all points of the domain that do not meet the constraint.
        red_points = []
        domain_corner_points = []

        # The very first point needs to be compared to the very last point (domain is a loop) to know if there is a
        # jump from 'remove' to 'no_remove' (or the other way around).
        last_point_state = ['remove', 'no_remove'][cs.func(self.domain[-1])]
        first_point_state = ['remove', 'no_remove'][cs.func(self.domain[0])]
        if last_point_state != first_point_state:
            domain_corner_points.append([*self.domain[0][:2], 0])
            print('this should not happen')

        previous_state = first_point_state
        # print(previous_state)
        for position, value in enumerate(self.domain):
            if cs.func(value) is False:
                new_state = 'remove'
                red_points.append(value)
            else:
                new_state = 'no_remove'

            if new_state != previous_state:
                domain_corner_points.append([*self.domain[position][:2], position])
            previous_state = new_state

        if self.debug:
            plt.title("Before cutting")
            plot_list(red_points, 'ro')
            plot_list(cs.array)
            self.plot_domain()

        # For the corner points, we need to find the closest pair in the cs.array:
        requirement_corner_points = []
        for c_point in domain_corner_points:
            best_distance = float('inf')  # This should be beatable...
            for cs_position, a_point in enumerate(cs.array):
                distance = sqrt((c_point[0] - a_point[0]) ** 2 + (c_point[1] - a_point[1]) ** 2)
                if distance < best_distance:
                    best_distance = distance
                    position_best_distance = cs_position
            requirement_corner_points.append([*cs.array[position_best_distance], position_best_distance])

        # Now we should have two sets of points: the corner points of the domain and those of the requirement line.
        if len(requirement_corner_points) == 2:
            # Let's first cut the requirements array:
            rcp = requirement_corner_points
            if rcp[0][2] < rcp[1][2]:
                insert_line = cs.array[rcp[0][2]: rcp[1][2] + 1]
            else:
                insert_line = cs.array[rcp[1][2]: rcp[0][2] + 1]

            if self.debug:
                plot_list(insert_line, label="insert line")

            # Next we shall cut the domain:
            dcp = domain_corner_points
            dom_position = dcp[0][2]
            just_before = self.domain[dom_position - 1]
            just_after = self.domain[dom_position + 1]

            if cs.func(just_before) is True:
                domain_before = self.domain[:dcp[0][2] + 1]
                domain_after = self.domain[dcp[1][2]:]
                if self.debug:
                    plot_list(domain_before, label="domain_before")
                    plot_list(domain_after, label="domain_after")
                    print(f"SHOULD BE False: {cs.func(just_after)}")
            else:
                domain_before = []
                domain_after = self.domain[dcp[0][2]: dcp[1][2]]
                if debug:
                    plot_list(domain_after, label="domain line")

            if self.debug:
                plt.legend()
                plt.show()

            # Sometimes, the insert line needs to be flipped.
            # Let's check the distance between the first point of the domain_after and both sides of insert_line
            first_distance = sqrt((insert_line[0][0] - domain_after[0][0]) ** 2 +
                                  (insert_line[0][1] - domain_after[0][1]) ** 2)
            second_distance = sqrt((insert_line[-1][0] - domain_after[0][0]) ** 2 +
                                   (insert_line[-1][1] - domain_after[0][1]) ** 2)

            if first_distance < second_distance:
                insert_line = insert_line[::-1]

            new_domain = domain_before + insert_line + domain_after
            self.domain = new_domain
            if self.debug:
                plt.title("After cutting")
                self.plot_domain()
        else:
            raise NotImplementedError("More than two cutting points is not yet implemented")
