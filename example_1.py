import matplotlib.pyplot as plt
from main import Constraint, ConstrainedOptimization


co = ConstrainedOptimization(x_min=-2, x_max=2, y_min=0, y_max=10)

req1 = Constraint(y={'expression': 'y>x', 'x_min': -0.1, 'x_max': 2.1, 'step': 0.001})
req2 = Constraint(y={'expression': 'y>=-x**3 -0.5', 'x_min': -2.1, 'x_max': 0, 'step': 0.001})
req3 = Constraint(y={'expression': 'y<=2*(x+2.2)**(0.5)', 'x_min': -2.2, 'x_max': 2.2, 'step': 0.001})

plt.title("Before")
co.plot_domain()

co.adapt_domain(req1)
co.adapt_domain(req2)
co.adapt_domain(req3)

plt.title("After")
co.plot_domain()
